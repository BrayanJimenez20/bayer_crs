<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="presentacion/css/estilos_menu.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <title>Bayer</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>
<style>
    html {
        background: url(presentacion/imagenes/FONDO.png) no-repeat fixed center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    form {
        background: url(presentacion/imagenes/LOGIN.png) top center no-repeat;
    }

    @media screen and (max-width:1000px) {
        html {
            background: url(presentacion/imagenes/FONDO.png) no-repeat fixed center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    }

    .texto {
        background: -webkit-linear-gradient(#92c24a, #5dc2e9);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        color: #0ba7e2;
        margin-top: 20px;
    }

    hr {
        margin: 15px;
        width: 40%;
        background: -webkit-linear-gradient(#92c24a, #5dc2e9);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        color: #0ba7e2;
    }
</style>

</head>

<body>
    <div style="width:20%" align="center">
        <img src="presentacion/imagenes/esquina.png" height="120px" style="margin-left:1%; margin-top:1%;" />
        <p class="texto" style="font-family:Arial, Helvetica, sans-serif; font-size:20px;"><b>CRS</b></p>
        <hr />
    </div>
    <!--<a href="presentacion/form_paciente_nuevo.php" target="inf">nuevo</a>
<a href="presentacion/form_paciente_seguimiento.php" target="inf">seguimiento</a>-->
    <center></center>

    <form id="inicio" action="logica/ini_sesion.php" method="POST" style="width:100%; margin-top:100px;">

        <section style="width:20%; height:100%; padding:0;  text-align:center; margin:auto auto;">
            <br />
            <br />
            <br />
            <br />

            <span class="fuente"><i class="fa fa-user" aria-hidden="true"></i> Usuario: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input style="width:250px;" id="usuario" name="usuario" type="text" required="required" title="ESCRIBA UN NOMBRE DE USUARIO" />
            <br />
            <br />
            <span class="fuente"><i class="fa fa-key" aria-hidden="true"></i> Contrase&ntilde;a: </span>
            <input style="width:250px;" id="Contrasena" name="Contrasena" type="password" required="required" title="ESCRIBA UNA CONTRASEÑA CORRECTA" />
            <br />
            <br />
            <br />
            <center>
                <input id="Inicio" name="Inicio" type="submit" value="INICIAR SESION" class="btn_iniar" />
                <br />
                <br />
            </center>
        </section>
    </form>

    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

    <center>

        <p style="font-family:Arial, Helvetica, sans-serif; color:#333333">Copyright &copy; <b> <?PHP echo date("Y") ?> <b><span style="color:#006699">People Marketing </span></b></p>
    </center>
</body>

</html>