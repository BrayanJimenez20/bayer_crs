// JavaScript Document

function mostrar_producto()
{
	var ID_PRODUCTO=$('#tipo_envio').val();	
	$.ajax(
	{
		url:'../../presentacion/mostrar_nombre_producto.php',
		data:
		{
			ID_PRODUCTO: ID_PRODUCTO,
		},
		type: 'post',
		beforeSend: function () 
		{
			$('#div_agregar').css('visibility','hidden');
		},
		success: function(data)
		{
			$('#nombre_producto').html(data);
			
			var nom=$('#nombre_producto').val();
			//alert(nom);
			if(nom=='Kit de bienvenida'||nom=='')
			{
				$('#div_agregar').css('visibility','hidden');
			}
			else
			{
				$('#div_agregar').css('visibility','visible');
			}
		}
	}
	)
}
//AGREGAR PRODUCTO
function agregar_producto()
{
	var ID_PRODUCTO=$('#tipo_envio').val();
	var ID_PACIENTE=$('#codigo_usuario2').val();
	var NOMBRE_PRODUCTO=$('#nombre_producto').val();
	$.ajax(
	{
		url:'../../presentacion/ingresar_productos_temporal.php',
		data:
		{
			ID_PRODUCTO: ID_PRODUCTO,
			ID_PACIENTE: ID_PACIENTE,
			NOMBRE_PRODUCTO:NOMBRE_PRODUCTO
		},
		type: 'post',
		beforeSend: function () 
		{
			$('#tabla_material_agregar').css('visibility','visible');
				$("#tabla_material_agregar").html("Procesando, espere por favor"+'<img src="imagenes/cargando.gif" />');
		},
		success: function(data)
		{
			
			//$('#div_tabla_productos').html('');
			
			$('#tabla_material_agregar').html(data);
		}
	}
	)
}
function materiales()
{
	var REFERENCIA=$('#producto_tratamiento').val();
	$.ajax(
	{
		url:'../../presentacion/listado_producto_registrar.php',
		data:
		{
			REFERENCIA: REFERENCIA
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#tipo_envio").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#tipo_envio").removeAttr('disabled');
			$('#tipo_envio').html(data);
		}
	})
}
function status()
{
	var REFERENCIA=$('#producto_tratamiento').val();
	$.ajax(
	{
		url:'../../presentacion/listado_producto_status.php',
		data:
		{
			REFERENCIA: REFERENCIA
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#status_paciente").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#status_paciente").removeAttr('disabled');
			$('#status_paciente').html(data);
		}
	})
}
function clasificacion()
{
	var REFERENCIA=$('#producto_tratamiento').val();
	$.ajax(
	{
		url:'../../presentacion/listado_clasificacion_patologica.php',
		data:
		{
			REFERENCIA: REFERENCIA
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#clasificacion_patologica").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#clasificacion_patologica").removeAttr('disabled');
			$('#clasificacion_patologica').html(data);
		}
	})
}
/*function asegurador()
{
	var DEPT=$('#departamento').val();	
	$.ajax(
	{
		url:'../presentacion/listado_asegurador.php',
		data:
		{
			DEPT: DEPT
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#asegurador").attr('disabled', 'disabled');
			$('#operador_logistico').html('');
			$("#operador_logistico").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#asegurador").removeAttr('disabled');
			$('#asegurador').html(data);
		}
	})
}*/
function operador()
{
	var DEPT=$('#departamento').val();
	var asegurador=$('#asegurador').val();
	$.ajax(
	{
		url:'../../presentacion/listado_operador_logistico.php',
		data:
		{
			DEPT: DEPT,
			asegurador: asegurador
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#operador_logistico").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#operador_logistico").removeAttr('disabled');
			$('#operador_logistico').html(data);
		}
	})
}
function mostrar_dosis()
{
	var reclamo=$('#reclamo').val();
	var MEDICAMENTO=$('#producto_tratamiento').val();
	if(reclamo=='SI'&& MEDICAMENTO=='BETAFERON CMBP X 15 VPFS (3750 MCG) MM')
	{
		$("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
		$("#consecutivo").val($('#consecutivo').prop('defaultValue'));
		$("#consecutivo_betaferon_span").css('display','block');
		$('#consecutivo_betaferon').css('display','block');
		
		$("#fecha_reclamacion_span").css('display','block');
		$('#fecha_reclamacion').css('display','block');
		
		$("#causa").css('display','none');
		$('#causa_no_reclamacion').css('display','none');
		$('#numero_cajas').removeAttr('disabled');
		$('#tipo_numero_cajas').removeAttr('disabled');
	}
	else
	{
		$("#consecutivo_betaferon_span").css('display','none');
		$('#consecutivo_betaferon').css('display','none');
	}

	var producto=$('#producto_tratamiento').val();
	//alert(producto);
	$.ajax(
	{
		url:'../presentacion/dosis.php',
		data:
		{
			producto: producto,
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#span_dosis").css('display','block');
			$("#span_dosis").html('<img src="imagenes/cargando.gif" />'+"  Procesando, espere por favor");
			$('#Dosis').attr('disabled');
			$("#Dosis").css('display','none');
		},
		success: function(data)
		{
			$("#Dosis option:eq(0)").attr("selected", "selected");
			$('#Dosis').val('');
			$('#Dosis3').val('');
			$("#span_dosis").css('display','none');
			$("#Dosis").css('display','block');
			$("#span_dosis").html("");		
			$('#Dosis').html(data);
			if(producto=='KOGENATE FS 2000 PLAN')
			{
				$('#Dosis3').css('display','block');
				$('#Dosis2').css('display','none');
				$('#Dosis').css('display','none');
			}
			if(producto=='Xofigo 1x6 ml CO')
			{
				$('#Dosis2').css('display','block');
				$('#Dosis3').css('display','none');
				$('#Dosis').css('display','none');
			}
			if(producto!='Xofigo 1x6 ml CO' && producto!='KOGENATE FS 2000 PLAN')
			{
				$('#Dosis').removeAttr("disabled");
				$('#Dosis2').css('display','none');
				$('#Dosis3').css('display','none');
				$('#Dosis').css('display','block');
			}
			//Dosis2
		}
	}
	)
}
