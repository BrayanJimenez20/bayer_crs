// JavaScript Document

/*DIRECCION*/
$(document).ready(function()
{
	function reclamo()
	{
		$("#causa_no_reclamacion option:eq(0)").attr("selected", "selected");
		$("#fecha_reclamacion").val('');
		
		var reclamo=$('#reclamo').val();
		var MEDICAMENTO=$('#producto_tratamiento').val();
		if(reclamo=='')
		{
			$("#causa").css('display','none');
			$('#causa_no_reclamacion').css('display','none');
			
			$("#fecha_reclamacion_span").css('display','none');
			$('#fecha_reclamacion').css('display','none');
			
			$("#consecutivo_betaferon_span").css('display','none');
			$('#consecutivo_betaferon').css('display','none');
			
			$('#numero_cajas option:eq(0)').attr('selected','selected');
			$('#tipo_numero_cajas option:eq(0)').attr('selected','selected');
			
			$('#numero_cajas').attr('disabled','disabled');
			$('#tipo_numero_cajas').attr('disabled','disabled');
		}
		if(reclamo=='NO')
		{
			$("#causa").css('display','block');
			$('#causa_no_reclamacion').css('display','block');
			
			$("#fecha_reclamacion_span").css('display','none');
			$('#fecha_reclamacion').css('display','none');
			
			$("#consecutivo_betaferon_span").css('display','none');
			$('#consecutivo_betaferon').css('display','none');
			
			$('#numero_cajas option:eq(0)').attr('selected','selected');
			$('#tipo_numero_cajas option:eq(0)').attr('selected','selected');
			$('#causa_no_reclamacion option:eq(1)').attr('selected','selected');
			
			$('#numero_cajas').attr('disabled','disabled');
			$('#tipo_numero_cajas').attr('disabled','disabled');
			
		}
		if(reclamo=='SI'&& MEDICAMENTO=='BETAFERON CMBP X 15 VPFS (3750 MCG) MM')
		{
			$("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
			$("#consecutivo").val($('#consecutivo').prop('defaultValue'));
			$("#consecutivo_betaferon_span").css('display','block');
			$('#consecutivo_betaferon').css('display','block');
			
			$("#fecha_reclamacion_span").css('display','block');
			$('#fecha_reclamacion').css('display','block');
			
			$("#causa").css('display','none');
			$('#causa_no_reclamacion').css('display','none');
			$('#numero_cajas').removeAttr('disabled');
			$('#tipo_numero_cajas').removeAttr('disabled');
		}
		else
		{
			if(reclamo=='SI')
			{
				$("#consecutivo_betaferon_span").css('display','none');
				$('#consecutivo_betaferon').css('display','none');
			
				$("#fecha_reclamacion_span").css('display','block');
				$('#fecha_reclamacion').css('display','block');
				
				$("#causa").css('display','none');
				$('#causa_no_reclamacion').css('display','none');
				$('#numero_cajas').removeAttr('disabled');
				$('#tipo_numero_cajas').removeAttr('disabled');
				$("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
			}
		}
	}
	reclamo();
	$("#reclamo").change(function()
	{
		reclamo();
	});
	$("#departamento").change(function()
	{
		//asegurador();
	});
	$("#asegurador").change(function()
	{
		operador();
	});
	$('#cambio').click(function()
	{
		$('#cambio_direccion').toggle();
		$('#DIRECCION').val('');		
		$("#VIA option:eq(0)").attr("selected", "selected");
		$("#interior option:eq(0)").attr("selected", "selected");
		$("#interior2 option:eq(0)").attr("selected", "selected");
		$("#interior3 option:eq(0)").attr("selected", "selected");
		$("#TERAPIA option:eq(0)").attr("selected", "selected");
		$('#detalle_via').val('');
		$('#detalle_int').val('');
		$('#detalle_int2').val('');
		$('#detalle_int3').val('');
		$('#numero').val('');
		$('#numero2').val('');

	});
	var via=$('#VIA').val();
	var dt_via=$('#detalle_via').val();
	$('#VIA').change(function()
	{
		dir();
	});
	
	$('#detalle_via').change(function()
	{
		dir();
	});
	$('#numero').change(function()
	{
		dir();
	});
	$('#numero2').change(function()
	{
		dir();
	});
	$('#interior').change(function()
	{
		dir();		
	});
	$('#detalle_int').change(function()
	{
		dir();
	});
	$('#interior2').change(function()
	{
		dir();		
	});
	$('#detalle_int2').change(function()
	{
		dir();
	});
	$('#interior3').change(function()
	{
		dir();		
	});
	$('#detalle_int3').change(function()
	{
		dir();
	});
	
	
});
/*FIN DIRECCION*/	
	
	
$(document).ready(function()
{
	
	var fecha=$('input[name=fecha_nacimiento]').val();
	if(fecha!='')
	{
		var edad=nacio(fecha);
		$("#edad").val(edad);
	}
	$("input[name=fecha_nacimiento]").change(function()
	{
		var fecha=$('input[name=fecha_nacimiento]').val();
		var edad=nacio(fecha);
		$("#edad").val(edad);
	});
	
	$("#medico").change(function()
	{
		$("#medico_nuevo").val('');
		
		var medico=$('#medico').val();
		if(medico=='Otro')
		{
			$('#medico_nuevo').css('display','inline-block');
			$('#cual_medico').css('display','inline-block');
		}
		if(medico!='Otro')
		{
			$('#medico_nuevo').css('display','none');
			$('#cual_medico').css('display','none');
		}

	});
	
	$("#producto_tratamiento").click(function mostrar_nebu()
	{	
		$("#nebulizaciones").val('');
		var producto_tratamiento=$('#producto_tratamiento').val();
		if(producto_tratamiento=='VENTAVIS 10 1SOL/2ML X30AMP(Conse) MM')
		{
			$('#span_nebulizaciones').css('display','inline-block');
			$('#div_nebulizaciones').css('display','inline-block');
		}
		if(producto_tratamiento!='VENTAVIS 10 1SOL/2ML X30AMP(Conse) MM')
		{
			$('#span_nebulizaciones').css('display','none');
			$('#div_nebulizaciones').css('display','none');
		}
	});
	
	$("#producto_tratamiento").click(function mostrar_tabletas()
	{	
		$("#numero_tabletas_diarias").val('');
		
		var reclamo=$('#reclamo').val();
		var producto_tratamiento=$('#producto_tratamiento').val();
		
		if(reclamo=='SI'&&producto_tratamiento=='NEXAVAR 200MGX60C(12000MG)INST'||producto_tratamiento=='ADEMPAS')
		{
			$('#span_tabletas_diarias').css('display','inline-block');
			$('#div_tabletas_diarias').css('display','inline-block');
		}
		if(producto_tratamiento!='NEXAVAR 200MGX60C(12000MG)INST'&&producto_tratamiento!='ADEMPAS')
		{
			$('#span_tabletas_diarias').css('display','none');
			$('#div_tabletas_diarias').css('display','none');
		}
		if(reclamo=='NO'||reclamo=='')
		{
			$('#span_tabletas_diarias').css('display','none');
			$('#div_tabletas_diarias').css('display','none');
		}
	});
	
	$("#reclamo").click(function mostrar_tabletas2()
	{	
		$("#numero_tabletas_diarias").val('');
		
		var reclamo=$('#reclamo').val();
		var producto_tratamiento=$('#producto_tratamiento').val();
		
		if(reclamo=='SI'&&producto_tratamiento=='NEXAVAR 200MGX60C(12000MG)INST'||producto_tratamiento=='ADEMPAS')
		{
			$('#span_tabletas_diarias').css('display','inline-block');
			$('#div_tabletas_diarias').css('display','inline-block');
		}
		if(producto_tratamiento!='NEXAVAR 200MGX60C(12000MG)INST'&&producto_tratamiento!='ADEMPAS')
		{
			$('#span_tabletas_diarias').css('display','none');
			$('#div_tabletas_diarias').css('display','none');
		}
		if(reclamo=='NO'||reclamo=='')
		{
			$('#span_tabletas_diarias').css('display','none');
			$('#div_tabletas_diarias').css('display','none');
		}
	});
	
	$("#producto_tratamiento").change(function()
	{
		$('nombre_producto').val('');
		mostrar_dosis();
	});
	$('#producto_tratamiento').change(function()
	{
		materiales();
		clasificacion();
		status();	
	});
	$("#tipo_envio").change(function()
	{
		mostrar_producto();
	});
	$("#agregar_nuevo").click(function()
	{
		$('#div_material_agregar').css('display','block');
		//$("#tipo_envio option:eq(0)").attr("selected", "selected");
		$('#div_agregar').css('visibility','hidden');
	});
});
