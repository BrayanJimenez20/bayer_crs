<?php
include('../logica/session.php');
header("Content-Type: text/html;charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="es">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Documento sin titulo</title>
	<link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
	<script src="css/SpryAssets/SpryAccordion.js" type="text/javascript"></script>
	<link href="css/SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
	<link href="css/estilo_form_paciente.css" type="text/css" />
	<script src="js/jquery.js"></script>
	<script type="text/javascript" src="js/direccion.js"></script>
	<script type="text/javascript" src="js/validar_campos_pacientes.js"></script>
	<script type="text/javascript" src="js/validaciones.js"></script>
	<script type="text/javascript" src="js/calcular_edad.js"></script>
	<script type="text/javascript" src="js/validar_caracteres.js"></script>
	<script type="text/javascript">
		function trat_previo(sel) {
			if (sel.value == "Otro") {

				divC = document.getElementById("otro_tratamiento");
				divC.style.display = "";

			}
			if (sel.value != "Otro") {

				divC = document.getElementById("otro_tratamiento");
				divC.style.display = "none";

			}
		}

		function mostrar_departamento() {
			var pais = $('#pais').val();
			$("#departamento").html('<img src="imgagenes/cargando.gif" />');
			$.ajax({
				url: '../presentacion/departamentos.php',
				data: {
					pais: pais,
				},
				type: 'post',
				beforeSend: function() {
					$('#departamento').attr('disabled');
					$("#departamento").html("Procesando, espere por favor" + '<img src="img/cargando.gif" />');
				},
				success: function(data) {
					$('#departamento').removeAttr("disabled");
					$('#departamento').html(data);
				}
			})
		}

		function mostrar_ciudades() {
			var departamento = $('#departamento').val();
			$("#ciudad").html('<img src="imgagenes/cargando.gif" />');
			$.ajax({
				url: '../presentacion/ciudades.php',
				data: {
					dep: departamento,
				},
				type: 'post',
				beforeSend: function() {
					$('#ciudad').attr('disabled');
					$("#ciudad").html("Procesando, espere por favor" + '<img src="img/cargando.gif" />');
				},
				success: function(data) {
					$('#ciudad').removeAttr("disabled");
					$('#ciudad').html(data);
				}
			})
		}
	</script>
	<script>
		/*DIRECCION*/
		$(document).ready(function() {
			$("#departamento").change(function() {
				//asegurador();
			});
			$("#asegurador").change(function() {
				operador();
			});
			$('#cambio').click(function() {
				$('#cambio_direccion').toggle();
				$('#DIRECCION').val('');
				$("#VIA option:eq(0)").attr("selected", "selected");
				$("#interior option:eq(0)").attr("selected", "selected");
				$("#interior2 option:eq(0)").attr("selected", "selected");
				$("#interior3 option:eq(0)").attr("selected", "selected");
				$("#TERAPIA option:eq(0)").attr("selected", "selected");
				$('#detalle_via').val('');
				$('#detalle_int').val('');
				$('#detalle_int2').val('');
				$('#detalle_int3').val('');
				$('#numero').val('');
				$('#numero2').val('');

			});
			var via = $('#VIA').val();
			var dt_via = $('#detalle_via').val();
			$('#VIA').change(function() {
				dir();
			});

			$('#detalle_via').change(function() {
				dir();
			});
			$('#numero').change(function() {
				dir();
			});
			$('#numero2').change(function() {
				dir();
			});
			$('#interior').change(function() {
				dir();
			});
			$('#detalle_int').change(function() {
				dir();
			});
			$('#interior2').change(function() {
				dir();
			});
			$('#detalle_int2').change(function() {
				dir();
			});
			$('#interior3').change(function() {
				dir();
			});
			$('#detalle_int3').change(function() {
				dir();
			});


		});
		/*FIN DIRECCION*/


		$(document).ready(function() {

			var fecha = $('input[name=fecha_nacimiento]').val();
			if (fecha != '') {
				var edad = nacio(fecha);
				$("#edad").val(edad);
			}
			$("input[name=fecha_nacimiento]").change(function() {
				var fecha = $('input[name=fecha_nacimiento]').val();
				var edad = nacio(fecha);
				$("#edad").val(edad);
			});


			$("#agregar_nuevo").click(function() {
				$('#div_material_agregar').css('display', 'block');
				//$("#tipo_envio option:eq(0)").attr("selected", "selected");
				$('#div_agregar').css('visibility', 'hidden');
			});
		});
	</script>
	<script type="text/javascript" src="../logica/js/campos_form_nuevo.js"></script>
	<script type="text/javascript" src="../logica/js/validaciones_form_nuevo.js"></script>
	<style>
		td {
			padding: 6px;
			background-color: transparent;
		}

		.input__row {
			margin-top: 10px;
		}

		/* Radio button */

		/* Upload button */
		.upload {
			display: none;
		}

		.uploader {
			border: 1px solid #12a9e3;
			width: 300px;
			position: relative;
			height: 30px;
			display: flex;
		}

		.uploader .input-value {
			width: 250px;
			padding: 5px;
			overflow: hidden;
			text-overflow: ellipsis;
			line-height: 25px;
			font-family: sans-serif;
			font-size: 16px;
		}

		.uploader label {
			cursor: pointer;
			margin: 0;
			width: 30px;
			height: 30px;
			position: absolute;
			right: 0;
			background: #17a8e391 url('https://www.interactius.com/wp-content/uploads/2017/09/folder.png') no-repeat center;
		}
	</style>
</head>
<?php
$string_intro = getenv("QUERY STRING");
parse_str($string_intro);
require('../datos/conex.php');
$DIAS_ANTES = date('Y-m-d', strtotime('-31 day')); // resta 31 d a	
if ($privilegios != '' && $usuname_peru != '') {
?>

	<body class="body" style="width:80.9%;margin-left:12%;">
		<form id="paciente_nuevo" name="paciente_nuevo" action="../logica/insertar_datos.php" method="post" enctype="multipart/form-data" class="letra">
			<div id="Accordion1" class="Accordion" tabindex="0">
				<div class="AccordionPanel">
					<div class="AccordionPanelTab" style="padding:5px"><strong>INFORMACION GENERAL</strong></div>
					<div class="AccordionPanelContent">
						<table width="100%" border="0">

							<tr>
								<td width="20%">
									<span>Codigo de Usuario</span>
								</td>
								<td width="30%">
									<?php
									$Seleccion = mysqli_query($conex, "SELECT ID_PACIENTE FROM `bayer_pacientes` WHERE ID_PACIENTE != '' ORDER BY ID_PACIENTE DESC LIMIT 1");
									while ($fila = mysqli_fetch_array($Seleccion)) {

										$ID_PA = $fila['ID_PACIENTE'] + 1;
										function Zeros($numero, $largo)
										{
											$resultado = $numero;
											while (strlen($resultado) < $largo) {
												$resultado = "0" . $resultado;
											}
											return $resultado;
										}
										$ID_PACIENTE = Zeros($ID_PA, 5);
									}
									?>
									<input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%;display:none" value="SI" checked="checked" />
									<input name="codigo_usuario" type="text" id="codigo_usuario" max="10" readonly="readonly" value="<?php echo 'CRS' . $ID_PACIENTE; ?>" />
									<input name="codigo_usuario2" type="hidden" id="codigo_usuario2" max="10" readonly="readonly" value="<?php echo $ID_PACIENTE; ?>" />
									<input name="usuname" id="usuname" value="<?php echo $usuname_peru; ?>" type="hidden" />
								</td>

								<td width="20%">
									<span>Estado del Paciente<span class="asterisco">*</span></span>
								</td>
								<td width="30%">
									<select type="text" name="estado_paciente" id="estado_paciente">
										<option value="">Seleccione...</option>
										<option>Proceso</option>
										<option>Activo</option>
										<option>Abandono</option>
									</select>
								</td>
							</tr>
							<tr>
								<td width="20%">
									<span>Fecha de Activacion<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td width="30%">
									<input type="date" name="fecha_activacion" id="fecha_activacion" value="<?php echo date('Y-m-d'); ?>" readonly="readonly" />
								</td>
								<td width="20%">
									<span>Nombre Completo<span class="asterisco">*</span></span>
								</td>
								<td width="30%">
									<input type="text" name="nombre" id="nombre" />
								</td>
							</tr>
							<tr>
								<td width="20%">
									<span>Numero Identificacion<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td width="30%">
									<input type="number" name="identificacion" id="identificacion" require />
								</td>

								<td width="20%">
									<span>Numero Celular<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td width="30%">
									<input type="number" name="telefono1" id="telefono1" require />
								</td>
							</tr>

							<tr>
								<td width="20%">
									<span>Numero Celular 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td width="30%">
									<input type="number" name="telefono2" id="telefono2" />
								</td>

								<td width="20%">
									<span>Numero Celular 3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td width="30%">
									<input type="number" name="telefono3" id="telefono3" />
								</td>
							</tr>

							<tr>
								<td><span>Correo Electronico<span class="asterisco">*</span></span></td>
								<td><input type="email" name="correo" id="correo" /></td>

								<td><span>Pais<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
								<td>
									<select type="text" name="pais" id="pais" onchange="mostrar_departamento()" style="text-transform:capitalize">
										<option value="">Seleccione...</option>
										<?php
										$Seleccion = mysqli_query($conex, "SELECT * FROM `bayer_pais`");
										while ($fila = mysqli_fetch_array($Seleccion)) {
											echo "<option value=\"" . $fila['ID_PAIS'] . "\">" . utf8_encode($fila['NOMBRE_PAIS']) . "</option>";
										}
										?>
									</select>
								</td>
							</tr>

							<tr>
								<td><span>Departamento<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
								<td><select type="text" name="departamento" id="departamento" onchange="mostrar_ciudades()" style="text-transform:capitalize">
										<option value="">Seleccione...</option>
									</select>
								</td>

								<td><span>Ciudad<span class="asterisco">*</span></span></td>
								<td><select type="text" name="ciudad" id="ciudad">
										<option value="">Seleccione...</option>
									</select>
								</td>
							</tr>

							<tr style="padding:3%;">
								<td style="width:10%;"><span>Direccion<span class="asterisco">*</span></span></td>
								<td bgcolor="#FFFFFF" colspan="3">
									<input type="text" name="DIRECCION" id="DIRECCION" style="width:98.5%;" />
								</td>
							</tr>
							<tr style="padding:3%;">
								<td><span>Via:</span></td>
								<td style="width:35%">
									<span>
										<select id="VIA" name="VIA" style="width:96%">
											<option value="">Seleccione...</option>
											<option>ANILLO VIAL</option>
											<option>AUTOPISTA</option>
											<option>AVENIDA</option>
											<option>BOULEVAR</option>
											<option>CALLE</option>
											<option>CALLEJON</option>
											<option>CARRERA</option>
											<option>CIRCUNVALAR</option>
											<option>CONDOMINIO</option>
											<option>DIAGONAL</option>
											<option>KILOMETRO</option>
											<option>LOTE</option>
											<option>SALIDA</option>
											<option>SECTOR</option>
											<option>TRANSVERSAL</option>
											<option>VEREDA</option>
											<option>VIA</option>
										</select>
									</span>
								</td>
								<td style="width:8%;"><span>Detalles Via:</span></td>
								<td width="177" bgcolor="#FFFFFF"><span>
										<input name="detalle_via" id="detalle_via" type="text" maxlength="15" style="width:95%" />
									</span>
								</td>
							</tr>
							<tr>
								<td width="96"><span>N&uacute;mero:</span></td>
								<td bgcolor="#FFFFFF">
									<span>
										<input name="numero" id="numero" type="text" maxlength="5" style="width:45%;" />
										-
										<input name="numero2" id="numero2" type="text" maxlength="5" style="width:45%;" />
									</span>
								</td>
								<td></td>
								<td bgcolor="#FFFFFF"></td>
							</tr>
							<tr style="padding:3%;">

								<td><span>Interior:</span></td>
								<td bgcolor="#FFFFFF"><span>
										<select id="interior" name="interior" style="width:96%">
											<option value="">Seleccione...</option>
											<option>APARTAMENTO</option>
											<option>BARRIO</option>
											<option>BLOQUE</option>
											<option>CASA</option>
											<option>CIUDADELA</option>
											<option>CONJUNTO</option>
											<option>CONJUNTO RESIDENCIAL</option>
											<option>EDIFICIO</option>
											<option>ENTRADA</option>
											<option>ETAPA</option>
											<option>INTERIOR</option>
											<option>MANZANA</option>
											<option>NORTE</option>
											<option>OFICINA</option>
											<option>OCCIDENTE</option>
											<option>ORIENTE</option>
											<option>PENTHOUSE</option>
											<option>PISO</option>
											<option>PORTERIA</option>
											<option>SOTANO</option>
											<option>SUR</option>
											<option>TORRE</option>
										</select>
									</span></td>
								<td><span>Detalles Interior:</span></td>
								<td bgcolor="#FFFFFF"><span>
										<input name="detalle_int" id="detalle_int" type="text" maxlength="30" readonly style="width:95%" />
									</span></td>

							</tr>
							<tr style="padding:3%;">
								<td><span>Interior:</span></td>
								<td bgcolor="#FFFFFF"><span>
										<select id="interior2" name="interior2" style="width:96%">
											<option value="">Seleccione...</option>
											<option>APARTAMENTO</option>
											<option>BARRIO</option>
											<option>BLOQUE</option>
											<option>CASA</option>
											<option>CIUDADELA</option>
											<option>CONJUNTO</option>
											<option>CONJUNTO RESIDENCIAL</option>
											<option>EDIFICIO</option>
											<option>ENTRADA</option>
											<option>ETAPA</option>
											<option>INTERIOR</option>
											<option>MANZANA</option>
											<option>NORTE</option>
											<option>OFICINA</option>
											<option>OCCIDENTE</option>
											<option>ORIENTE</option>
											<option>PENTHOUSE</option>
											<option>PISO</option>
											<option>PORTERIA</option>
											<option>SOTANO</option>
											<option>SUR</option>
											<option>TORRE</option>
										</select>
									</span></td>
								<td><span>Detalles Interior:</span></td>
								<td bgcolor="#FFFFFF"><span>
										<input name="detalle_int2" id="detalle_int2" type="text" maxlength="30" readonly style="width:95%" />
									</span></td>

							</tr>
							<tr style="padding:3%;">
								<td><span>Interior:</span></td>
								<td bgcolor="#FFFFFF"><span>
										<select id="interior3" name="interior3" style="width:96%">
											<option value="">Seleccione...</option>
											<option>APARTAMENTO</option>
											<option>BARRIO</option>
											<option>BLOQUE</option>
											<option>CASA</option>
											<option>CIUDADELA</option>
											<option>CONJUNTO</option>
											<option>CONJUNTO RESIDENCIAL</option>
											<option>EDIFICIO</option>
											<option>ENTRADA</option>
											<option>ETAPA</option>
											<option>INTERIOR</option>
											<option>MANZANA</option>
											<option>NORTE</option>
											<option>OFICINA</option>
											<option>OCCIDENTE</option>
											<option>ORIENTE</option>
											<option>PENTHOUSE</option>
											<option>PISO</option>
											<option>PORTERIA</option>
											<option>SOTANO</option>
											<option>SUR</option>
											<option>TORRE</option>
										</select>
									</span></td>
								<td><span>Detalles Interior:</span></td>
								<td bgcolor="#FFFFFF"><span>
										<input name="detalle_int3" id="detalle_int3" type="text" maxlength="30" style="width:95%" readonly />
									</span></td>

							</tr>


						</table>

					</div>
				</div>


				<div class="AccordionPanel">
					<div class="AccordionPanelTab" style="padding:5px"><strong>INFORMACION PERSONAL</strong></div>
					<div class="AccordionPanelContent">
						<table width="100%" border="0">
							<tr>
								<td while="20%">
									<span>Genero<span class="asterisco">*</span></span>
								</td>
								<td while="30%">
									<select name="genero" id="genero">
										<option value="">Seleccione...</option>
										<option value="hombre">Hombre</option>
										<option value="mujer">Mujer</option>
									</select>
								</td>

								<td while="20%"></td>
								<td while="30%"></td>
							</tr>
							<tr>
								<td while="20%">
									<span>Fecha de nacimiento<span class="asterisco">*</span></span>
								</td>
								<td while="30%">
									<input type="date" name="fecha_nacimiento" id="fecha_nacimiento">
								</td>

								<td while="20%"></td>
								<td while="30%"></td>
							</tr>
						</table>
					</div>
				</div>


				<div class="AccordionPanel">
					<div class="AccordionPanelTab" style="padding:5px"><strong>INFORMACION DE TRATAMIENTO</strong></div>
					<div class="AccordionPanelContent">
						<table width="100%" border="0">
							<tr>
								<td while="20%">
									<span>Producto<span class="asterisco">*</span></span>
								</td>
								<td while="30%">
									<select name="producto_tratamiento" id="producto_tratamiento">
										<option>Seleccione...</option>
										<option value="Mirena">Mirena</option>
										<option value="Kyleena">Kyleena</option>
										<option value="Jaydess">Jaydess</option>
									</select>
								</td>
								<td while="20%"></td>
								<td while="30%"></td>

							</tr>
							<tr>
								<td while="20%">
									<span>Consentimiento<span class="asterisco">*</span></span>
								</td>
								<td while="30%">
									<select name="consentimiento" id="consentimiento">
										<option>Seleccione...</option>
										<option value="Fisico">Fisico</option>
										<option value="Verbal">Verbal</option>
									</select>
								</td>

								<td while="20%"></td>
								<td while="30%"></td>
							</tr>
						</table>
						<input type="hidden" name="regimen" id="regimen" value="Particular" />
						<input type="hidden" name="asegurador" id="asegurador" value="Particular" />
					</div>
				</div>



				<div class="AccordionPanel">
					<div class="AccordionPanelTab" style="padding:5px"><strong>NOTAS Y ADJUNTOS</strong></div>
					<div class="AccordionPanelContent">
						<br />
						<br />
						<div style="width:91.4%;margin-left: 15px; ">
							<textarea class="tf w-input" style="width:100%; height:100px" id="txtCurp" name="nota" maxlength="5000" onkeypress="return check(event)" placeholder="Nota" id="test"></textarea>

						</div>
						<br />
						<br />

						<div style=" display: flex;  flex-wrap: wrap; margin-left: 15px; ">
							<div style="flex: 0 0 50%;  max-width: 50%;">
								<span>Tipo de Adjunto<span class="asterisco"></span></span>
								<input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%; display:none" value="" checked="checked" />
								<input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="Certificado de Solicitud de Reposicion" />Certificado de Solicitud de Reposicion
								<input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="CI" />CI
								<div class="input__row uploader">
									<div id="inputval" class="input-value"></div>
									<label for="archivo"></label>
									<input type="file" class="upload" name="archivo" id="archivo" class="aceptar">
								</div>

								<script>
									$('#archivo').on('change', function() {
										$('#inputval').text($(this).val());
									});
								</script>
							</div>

						</div>
						<div style="flex: 0 0 33.333333%;
    max-width: 33.333333%;"></div>
						<div style="flex: 0 0 33.333333%;
    max-width: 33.333333%;"></div>

						<center>
							<?php
							if ($privilegios != 5) {
							?>
								<input id="registrar" name="registrar" type="submit" value="REGISTRAR" class="btn_registrar" onClick="return validar(paciente_nuevo,1)" />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
							<?PHP
							}
							?>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript">
			function check(e) {
				tecla = (document.all) ? e.keyCode : e.which;

				//Tecla de retroceso para borrar, siempre la permite
				if (tecla == 8) {
					return true;
				}

				// Patron de entrada, en este caso solo acepta numeros,letras, guiones, puntos, parentesis y comas.
				patron = /[A-Za-z0-9-.,)(.,]/;
				tecla_final = String.fromCharCode(tecla);
				return patron.test(tecla_final);
			}
			var Accordion1 = new Spry.Widget.Accordion("Accordion1");
		</script>
	</body>
<?php
} else {
?>
	<script type="text/javascript">
		window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
	</script>
<?php
}
?>

</html>