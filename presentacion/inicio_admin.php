<?php
include('../logica/session.php')
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Documento sin titulo</title>
    <link rel="stylesheet" href="css/menu.css" />
    <link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
    <link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
    <link rel="stylesheet" href="../presentacion/css/menu.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<script src="js/jquery.js"></script>
<script src="../presentacion/js/jquery.js"></script>
<script>
    var height = window.innerHeight - 2;
    var porh = (height * 74 / 100);
    $(document).ready(function() {
        $('#info').css('height', porh);
    });
</script>
<style>
    html {
        background: url(../presentacion/imagenes/FONDO.png) no-repeat fixed center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>
<?php
if ($privilegios != '' && $usuname_peru != '') {
?>

    <body>
        <div class="body">
            <!--<div style="display:inline-block;margin-left:1%; margin-top:1%;">
    <img src="../presentacion/imagenes/esquina.png" height="120" style=""/>
    </div>
     <div class="div_menu" style="margin-top:-29px;" >-->
            <div class="div_menu" style="margin-top:-38px;">
                <ul>
                    <li><a style="font-family:Arial, Helvetica, sans-serif" href="../presentacion/form_paciente_nuevo.php" target="info"><i class="fa fa-user-plus" aria-hidden="true"></i> NUEVO MEDICO</a>
                    </li>
                    <li><a style="font-family:Arial, Helvetica, sans-serif" href="../presentacion/form_paciente_seguimiento.php" target="info"><i class="fa fa-search" aria-hidden="true"></i> SEGUIMIENTO </a>
                    </li>
                    <?php if ($usuname_peru != 'RVILLAVICENCIO') { ?>
                        <li>
                            <a style="font-family:Arial, Helvetica, sans-serif" href="../presentacion/reportes.php" target="info">
                                <i class="fa fa-file-text" aria-hidden="true"></i> REPORTES
                            </a>
                            <!--<ul>
                      <li>
                      	<a href="../informes/index.php" target="info"><span class="icon-unlocked"></span> OTROS REPORTES</a>
                      </li>
                    </ul>-->
                        </li>
                    <?php }    ?>
                    <li><a style="font-family:Arial, Helvetica, sans-serif" href="#"><i class="fa fa-cog" aria-hidden="true"></i> CONFIGURACION</a>
                        <ul>
                            <li><a style="font-family:Arial, Helvetica, sans-serif" href="../presentacion/operadores_registro.php" target="info"><i class="fa fa-plus-circle" aria-hidden="true"></i> CREACION EPS/OPL</a></li>
                            <li><a style="font-family:Arial, Helvetica, sans-serif" href="../presentacion/form_usuarios.php" target="info"><i class="fa fa-users" aria-hidden="true"></i> USUARIOS</a></li>
                            <li><a style="font-family:Arial, Helvetica, sans-serif" href="../presentacion/form_cuenta_usuario.php" target="info"><i class="fa fa-user" aria-hidden="true"></i> MI CUENTA</a></li>
                        </ul>
                    </li>
                    <li class="a1" id="salir"><a href="../logica/cerrar_sesion.php" style="font-family:Arial, Helvetica, sans-serif; border-right:2px solid transparent;"><i class="fa fa-power-off" aria-hidden="true"></i></a></li>
            </div>
        </div>
        <div class="body">
            <iframe style=" padding-top:20px; width:100%;border:1px solid transparent" name="info" id="info" scrolling="auto"></iframe>
        </div>
    </body>
<?php
} else {
?>
    <script type="text/javascript">
        window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
    </script>
<?php
}
?>

</html>