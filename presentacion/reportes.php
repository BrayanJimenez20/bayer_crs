<!DOCTYPE html>
<html lang="es">
<!-- META-->
<title>Reportes </title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- AJAX-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- BOOOTSTRAP -->
<link href="../presentacion/css/bootstrap_reporte.css" rel="stylesheet" />
<!-- JQUERY PAGINIADO-->
<script type="text/javascript" src="../presentacion/js/reporte_gestion.js"></script>
<!-- EXPORTABLE-->
<link href="../presentacion/css/style_reporte_gestion.css" rel="stylesheet" type="text/css">
<!--ESTILO DEL PAGINIADO-->
<link rel="stylesheet" type="text/css" href="../presentacion/css/jquery_reporte_gestion.css" />
<link href="../presentacion/css/estilo_menu_reporte_gestion.css" rel="stylesheet" type="text/css">
<!-- FUNCION DEL PAGINIADO -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#usertable').DataTable();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#SELECTOR').change(function() {
            var x = $('#SELECTOR').val();
            if (x == '<' || x == '>' || x == '') {
                $("#division1").css('display', 'inline');
                $("#division2").css('display', 'none');
            }
            if (x == 'total') {
                $("#division1").css('display', 'none');
                $("#division2").css('display', 'none');
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Busqueda').click(function() {
            $("#super_busqueda").css('display', 'inline-block');
            $("#ocultar_boton").css('display', 'none');
        });
        $('#Cancelar').click(function() {
            $("#super_busqueda").css('display', 'none');
        });
    });
</script>
</head>

<body>
    <div class="body">
        <div class="container" style="margin-top:-35px;">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Reporte Pacientes</h2>
                    <iframe src="../SCRIPTCASE/Bayer_CRS_20210118111851/" frameborder="0" width="100%" height="1000"></iframe>
                </div>
            </div>
        </div>
    </div>
    <?php /* } */ ?>
    <footer class="footer fixed-bottom">
        <div class="container">
            <span class="text-muted"></span>
        </div>
    </footer>
</body>

</html>
<!-- JQUERY EXPORTABLE--->
<script src="../presentacion/js/jquery-1.12.4.min.js"></script>
<!-- Llamar a los complementos javascript EXPORTABLE-->
<script src="../presentacion/js/FileSaver.min.js"></script>
<script src="../presentacion/js/Blob.min.js"></script>
<script src="../presentacion/js/xls.core.min.js"></script>
<script src="../presentacion/js/tableexport.js"></script>
<!-- JS DE PAGINIADO-->
<script type="text/javascript" src="../presentacion/js/jquery.dataTables.js"></script>
<!-- FUNCION DE LA EXPORTACION-->
<script>
    $("table").tableExport({
        formats: ["xlsx", "txt", "csv"], //Tipo de archivos a exportar ("xlsx","txt", "csv", "xls")
        position: 'button', // Posicion que se muestran los botones puedes ser: (top, bottom)
        bootstrap: false, //Usar lo estilos de css de bootstrap para los botones (true, false)
        fileName: "pad_reporte_gestiones", //Nombre del archivo 
    });
</script>